# Automating Java Projects with Gitlab

Demo Java project to demonstrate use of Maven, JUnit, Gitlab CI and Checkstyle.

## Maven Commands

| Command | Description |
|---------|-------------|
| `mvn clean` | Clean output folder |
| `mvn compile` | Compile the code |
| `mvn test` | Run all unit tests |
| `mvn checkstyle:check` | Run style checks |
| `mvn site` | Generate project report (includes checkstyle report) |

## Checkstyle

Checkstyle has been setup to use the file under `src/main/resources/checkstyle.xml`.
This file is the Sun style guide file from the Checkstyle project [[link]](https://raw.githubusercontent.com/checkstyle/checkstyle/master/src/main/resources/sun_checks.xml).
